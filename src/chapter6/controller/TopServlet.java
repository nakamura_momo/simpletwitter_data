package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isShowMessageForm = false;
		boolean isShowCommentForm = false;

		User user = (User) request.getSession().getAttribute("loginUser");

		if (user != null) {
			isShowMessageForm = true;
			isShowCommentForm = true;
		}

		/*
		 * String型のuser_idの値をrequest.getParameter("user_id")で
		 * JSPから受け取るように設定
		 * MessageServiceのgetMessageに引数としてString型のuser_idを追加
		 */
		String userId = request.getParameter("user_id");
		String start = request.getParameter("start");
		String end = request.getParameter("end");

		List<UserMessage> messages = new MessageService().select(userId, start, end);
		List<UserComment> comments = new CommentService().select();

		//入力した日付の保持
		request.setAttribute("startDate", start);
		request.setAttribute("endDate", end);

		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.setAttribute("isShowCommentForm", isShowCommentForm);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

}