package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String id = request.getParameter("messageId");

		//パラメータチェック
		//IDが数値以外の場合
		if(StringUtils.isEmpty(id) || !id.matches("^\\d*$")) {
			session.setAttribute("errorMessages", "不正なパラメータが入力されました");
			response.sendRedirect("./");
			return;
		}

		int messageId = Integer.parseInt(id);
		Message message = new MessageService().select(messageId);

		//パラメータチェック
		//つぶやきの内容が存在しない＝IDが範囲外の場合
		if(message == null) {
			session.setAttribute("errorMessages", "不正なパラメータが入力されました");
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		String text = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		Message message = new Message();
		message.setText(text);
		message.setId(messageId);

		if (!isValid(text, errorMessages)) {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		new MessageService().edit(message);
		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
