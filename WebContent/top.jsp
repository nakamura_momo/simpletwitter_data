<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<!-- ログインしている場合、ユーザー情報を表示 -->
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>

		<!-- エラー表示 -->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<!-- つぶやき絞り込み機能追加 -->
		<div class="filter-area">
			<form action="" method="get">
				日付：<input type="date" name="start" value="${startDate}">～ <input
					type="date" name="end" value="${endDate}"> <input
					type="submit" value="絞込">
			</form>
		</div>

		<!-- つぶやき機能 -->
		<!-- テキストボックス作成 -->
		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<!-- メッセージの表示 -->
					<div class="account-name">
						<span class="account"> <a
							href="./?user_id=<c:out value="${message.userId}"/> "> <c:out
									value="${message.account}" />
						</a>
						</span> <span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="text"><pre style="font-size: 17px;"><c:out value="${message.text}" /></pre>
					</div>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<!-- 削除ボタン追加 -->
					<!-- つぶやきのアカウントとユーザのアカウントが一致 -->
					<c:if test="${message.userId == loginUser.id}">
						<form action="deleteMessage" method="post">
							<!-- IDをservletで使用 -->
							<input type="hidden" name="messageId" value="${message.id}">
							<input id="click" type="submit" value="削除">
							<script type="text/javascript" src="${pageContext.request.contextPath}/js/dialog.js"></script>
						</form>
					</c:if>

					<!-- 編集ボタン追加 -->
					<!-- つぶやきのアカウントとユーザのアカウントが一致 -->
					<c:if test="${message.userId == loginUser.id}">
						<form action="edit" method="get">
							<!-- IDをservletで使用 -->
							<input type="hidden" name="messageId" value="${message.id}">
							<input type="submit" value="編集">
						</form>
					</c:if>
				</div>

				<!-- 返信機能追加 -->
				<div class="comment">
					<!-- テキストボックス作成（IDをservletで使用） -->
					<div class="form-area">
						<c:if test="${ isShowCommentForm }">
							<form action="comment" method="post">
								<br />
								<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
								<br /> <input type="submit" value="返信">（140文字まで）
								<!-- IDをservletで使用 -->
								<input type="hidden" name="messageId" value="${message.id}">
							</form>
						</c:if>
					</div>
					<!-- コメント表示 -->
					<c:forEach items="${comments}" var="comment">
						<!-- つぶやきに対するメッセージを表示 -->
						<c:if test="${message.id == comment.messageId}">
							<div class="account-name">
								<span class="account"> <c:out value="${comment.account}" /></span>
								<span class="name"><c:out value="${comment.name}" /></span>
							</div>
							<div class="text">
								<pre style="font-size: 17px;"><c:out value="${comment.text}" /></pre>
							</div>
							<div class="date">
								<fmt:formatDate value="${comment.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
						</c:if>
					</c:forEach>
				</div>

			</c:forEach>
		</div>

		<div class="copyright">Copyright(c)Nakamura Momoka</div>

	</div>
</body>
</html>
